/*
 * Copyright (C) 2018 Daniel Martín
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "fbf.h"

#define FBF_NULL_CONTENT_ERROR -1
#define FBF_MEMORY_ALLOC_ERROR -2
#define FBF_SYNTAX_ERROR -3

char *fbf_error[] = { 
	"Given content points to NULL",
	"Memory could not be allocated",
	"Illegal Syntax: Unexpected ']' found"
};

#define VERSION "0.2"

static void printError(int code)
{
	if (code < 0)
		code *= -1;

	if (code)
		printf("\nERROR: %s\n", fbf_error[code-1]);
}

static void xfree(void *p)
{
	if (p)
		free(p);
}

static char *read_file_content(char *path)
{
	FILE *f = fopen(path, "rb");
	if (!f) {
		printf("ERROR: '%s' Not Found!\n", path);
		return NULL;
	}
	fseek(f, 0, SEEK_END);
	long fsize = ftell(f);
	rewind(f);

	char *fc = calloc(1, fsize + 1);
	if (fc)
		fread(fc, fsize, 1, f);
	fclose(f);
	return fc;
}

static void _help()
{
	printf("FastBrainFuck v%s\n\n", VERSION);
}

static void _version()
{
	printf("FastBrainFuck v%s\n\nWritten by Daniel Martin." \
		" Please submit any comment or bug at dalme.net or at GitLab" \
		" [https://gitlab.com/DalmeGNU/fbf]\n", VERSION);
}

int main(int argc, char **argv)
{
	char *bf_file = NULL;
	for (int i = 1; i < argc; i++) {
		if (!strcmp(argv[i], "--help")) {
			_help();
			return 0;
		}
		if (!strcmp(argv[i], "--version")) {
			_version();
			return 0;
		}
		else if (argv[i][0] != '-') {
			if (!bf_file)
				bf_file = &argv[i][0];
			else
				printf("Warning: Source file '%s' is ignored\n", argv[i]);
		}
	}

	if (!bf_file) {
		printf("ERROR: Missing input files\n");
		return -1;
	}
	char *bf_code = read_file_content(bf_file);
	int ret = fbf(bf_code);
	if (ret < 0)
		printError(ret);

	xfree(bf_code);
	return ret;
}