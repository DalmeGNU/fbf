/*
 * Copyright (C) 2018 Daniel Martín
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FBF_ARRAY_SIZE 30000

char *bf_ptr;

static char *findEndOfLoop(char *c)
{
	if (*c == '[')
		c++;

	int i;
	for (i = 1; i && c; c++) {
		if (*c == '[')
			i++;
		else if (*c == ']')
			i--;
	}

	if (i)
		return NULL;

	return c - 1;
}

static int runCode(char *bf_code);

static int runLoop(char *bf_code)
{
	char *loop_end = findEndOfLoop(bf_code);
	if (!loop_end)
		return -3;
	unsigned int loop_code_size = loop_end - bf_code;
	char *loop_code = calloc(1, loop_code_size);
	if (!loop_code)
		return -2;

	memcpy(loop_code, bf_code + 1, loop_code_size - 1);

	while (*bf_ptr) {
		int ret = runCode(loop_code);
		if (ret < 0)
			return ret;
	}
	free(loop_code);
	return 0;
}

static int runCode(char *bf_code)
{
/*	if (!bf_code)
		return -1;*/

	int ret = 0;
	while (*bf_code) {
		switch (*bf_code) {
		case '>':
			bf_ptr++;
			break;
		case '<':
			bf_ptr--;
			break;
		case '+':
			(*bf_ptr)++;
			break;
		case '-':
			(*bf_ptr)--;
			break;
		case '.':
			putchar(*bf_ptr);
			break;
		case ',':
			*bf_ptr = getchar();
			break;
		case '[':
			ret = runLoop(bf_code);

			if (ret < 0)
				return ret;
			bf_code = findEndOfLoop(bf_code);
			break;
		case ']':
			return -3;
			break;
		}
		bf_code++;
	}
	return 0;
}

int fbf(char *bf_code)
{
	if (!bf_code)
		return -1;

	char *bf_array = calloc(1, FBF_ARRAY_SIZE);
	if (!bf_array)
		return -2;

	bf_ptr = &bf_array[0];
	int ret = runCode(bf_code);
	free(bf_array);
	return ret;
}