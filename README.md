## FastBrainFuck

FastBrainFuck (fbf) is a simple and fast BrainFuck interpreter written in C

### How to compile FastBrainFuck

It doesn't need any dependencies (just some standard libc functions):

      $ cd src && gcc -std=c99 main.c fbf.c -o fbf && cd ..

### Execute

      $ ./src/fbf path_to_code.bf
